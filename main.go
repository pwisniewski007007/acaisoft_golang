
// main.go
package main

import (
    "encoding/json"
    "log"
    "io/ioutil"
    "net/http"
    "strconv"
    "math/rand"
    "github.com/gorilla/mux"
    "time"
)

type Message struct {
    Id      string    `json:"Id"`
    Email   string `json:"email"`
    Title    string `json:"title"`
    Content string `json:"content"`
    Code string `json:"code"`
}

var Messages []Message

func returnAllMessages( w http.ResponseWriter, r *http.Request ) {
    json.NewEncoder( w ).Encode( Messages ) 
}

func deleteMessage( w http.ResponseWriter, r *http.Request ) {
    vars := mux.Vars( r )
    idKey := vars["id"]
    codeKey := vars["code"]

    for index, message := range Messages {
        if message.Code == codeKey && message.Id == idKey {
            Messages = append( Messages[:index], Messages[index+1:] ... )
        }
    }
}

func returnSingleMessage( w http.ResponseWriter, r *http.Request ) {
    key := r.URL.Query().Get( "email" )
    var retMessages []Message
    for _, message := range Messages {
        if message.Email == key {
            retMessages = append( retMessages, message )
        }
    }
     json.NewEncoder( w ).Encode( retMessages )
}

func createNewMessage( w http.ResponseWriter, r *http.Request ) {
    rand.Seed(time.Now().UTC().UnixNano())

    reqBody, _ := ioutil.ReadAll( r.Body )
    var message Message 
    json.Unmarshal( reqBody, &message )

    message.Id= strconv.Itoa( len( Messages ) + 1 )
    message.Code = strconv.Itoa( rand.Intn( 255 ) ) //random number 0-255

    Messages = append( Messages, message )
    json.NewEncoder( w ).Encode( message.Code ) 
}

func handleRequests() {
    myRouter := mux.NewRouter().StrictSlash( true )

    myRouter.HandleFunc( "/messages", returnAllMessages )
    myRouter.HandleFunc( "/api/messages/{id}/{code}", deleteMessage ).Methods( "DELETE" )
    myRouter.HandleFunc( "/api/messages", createNewMessage ).Methods( "POST" )
    myRouter.HandleFunc( "/api/messages", returnSingleMessage ).Methods( "GET" )

    log.Fatal(http.ListenAndServe( ":8080", myRouter ) )
}

func main() {
    handleRequests()
}